//
//  RadialScene.m
//  cocos
//
//  Created by Stojce Slavkovski on 5/18/13.
//  Copyright (c) 2013 nextsense. All rights reserved.
//

#import "RadialScene.h"
#import "Base8Animator.h"

@interface RadialScene() {
    CGPoint center;
    CCMotionStreak *streak_;
    CCNode *target;
}

@property (strong, nonatomic)Base8Animator *anim;

@end

@implementation RadialScene

+(id) scene
{
    CCScene *scene = [CCScene node];
    RadialScene *layer = [RadialScene node];
    [scene addChild:layer];
    return scene;
}

- (void)onEnter
{
    [super onEnter];
    
    // the root object just rotates around
    CCNode *centerNode = [[CCNode alloc]init];
    [centerNode setPosition: ccp(center.x, center.y)];
    [self addChild:centerNode z:1];
    
	// the root object just rotates around
    CCNode *root = [[CCNode alloc]init];
    [centerNode addChild:root];
	[root setPosition:ccp(30,0)];
    
	// the target object is offset from root, and the streak is moved to follow it
    //	target = [CCSprite spriteWithFile:@"dotcolored.png"];
    target = [[CCNode alloc] init];
	[root addChild:target];
	[target setPosition:ccp(70,0)];
    
	// create the streak object and add it to the scene
    //	streak_ = [CCMotionStreak streakWithFade:20 minSeg:3 width:32 color:ccGREEN textureFilename:@"streak.png"];
    streak_ = [CCMotionStreak streakWithFade:15 minSeg:1 width:2 color:ccWHITE textureFilename:@"streak.png"];
	[self addChild:streak_];
    
	// schedule an update on each frame so we can syncronize the streak with the target
	[self schedule:@selector(onUpdate:)];
    
	id a1 = [CCRotateBy actionWithDuration:2.7f angle:360];
	id action1 = [CCRepeatForever actionWithAction:a1];
    
	id a2 = [CCRotateBy actionWithDuration:7.4f angle:360];
	id action2 = [CCRepeatForever actionWithAction:a2];
    
	[centerNode runAction:[CCRepeatForever actionWithAction:action2]];
	[root runAction:[CCRepeatForever actionWithAction:action1]];

    CCActionInterval *colorAction = [CCRepeatForever actionWithAction:[CCSequence actions:
                                                                       [CCTintTo actionWithDuration:1.5f red:0 green:255 blue:0],
                                                                       [CCTintTo actionWithDuration:1.5f red:49 green:164 blue:131],
                                                                       [CCTintTo actionWithDuration:1.5f red:160 green:222 blue:222],
                                                                       [CCTintTo actionWithDuration:1.5f red:0 green:0 blue:255],
                                                                       [CCTintTo actionWithDuration:1.5f red:131 green:213 blue:213],
                                                                       [CCTintTo actionWithDuration:1.5f red:102 green:143 blue:204],
                                                                       [CCTintTo actionWithDuration:1.5f red:255 green:0 blue:0],
                                                                       [CCTintTo actionWithDuration:1.5f red:26 green:255 blue:203],
                                                                       [CCTintTo actionWithDuration:1.5f red:102 green:204 blue:21],
                                                                       nil]
                                     ];
    [streak_ runAction:colorAction];
}

- (void)onUpdate:(ccTime)delta
{
	[streak_ setPosition:[target convertToWorldSpace:CGPointZero]];
}

// on "init" you need to initialize your instance
-(id) init
{
    self = [super initWithColor:ccc4(247, 249, 249, 255)];
    //    self = [super init];
    center = CGPointMake(160, 215);
	return self;
}

@end
