//
//  Base8Animator.h
//  cocos
//
//  Created by Stojce Slavkovski on 5/18/13.
//  Copyright (c) 2013 nextsense. All rights reserved.
//

typedef struct _color4F {
	GLfloat r;
	GLfloat g;
	GLfloat b;
	GLfloat a;
} color4F;

@interface Base8Animator : NSObject

- (id)initWithCenter:(CGPoint)center andIndex:(int)index ofTotal:(int)total;

@property (readonly)NSArray *drawPoints;

@end
