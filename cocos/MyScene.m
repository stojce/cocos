//
//  MyScene.m
//  cocos
//
//  Created by Stojce Slavkovski on 5/14/13.
//  Copyright (c) 2013 nextsense. All rights reserved.
//

#import "MyScene.h"
#import "Base8Animator.h"

@interface MyScene() {
    int counter;
    int animIndex;
    NSArray *dots;
    CGPoint center;
    NSArray *dots1;
    NSArray *dots2;
    NSArray *dots3;
    NSArray *dots4;
    NSArray *dots5;
    NSArray *dots6;
    NSArray *dots7;
    NSArray *dots8;
    
    
	CCNode *root;
    CCNode *target;
    
	CCMotionStreak *streak_;
}

@property (strong, nonatomic)Base8Animator *anim;

@end

@implementation MyScene

+(id) scene
{
    CCScene *scene = [CCScene node];
    MyScene *layer = [MyScene node];
    [scene addChild:layer];
    return scene;
}

- (void)onEnter
{
    [super onEnter];
    
    /*
//    streak_ = [CCMotionStreak streakWithFade:2 minSeg:3 width:32 color:ccGREEN textureFilename:@"dot.png"];
//	[self addChild:streak_];
    [self schedule:@selector(onUpdate:)];
    
    root = [CCSprite spriteWithFile:@"dot.png"];
    [root setPosition:CGPointMake(160, 210)];

    id a1 = [CCMoveTo actionWithDuration:2 position:CGPointMake(300, 300)];

    [root runAction:a1];
    
    */
    
    // the root object just rotates around
    CCNode *centerNode = [[CCNode alloc]init];
    [centerNode setPosition: ccp(center.x, center.y)];
    [self addChild:centerNode z:1];
    
	// the root object just rotates around
    root = [[CCNode alloc]init];
    [centerNode addChild:root];
	[root setPosition:ccp(25,0)];
    
	// the target object is offset from root, and the streak is moved to follow it
//	target = [CCSprite spriteWithFile:@"dotcolored.png"];
    	target = [[CCNode alloc] init];
	[root addChild:target];
	[target setPosition:ccp(60,0)];
    
	// create the streak object and add it to the scene
    //	streak_ = [CCMotionStreak streakWithFade:20 minSeg:3 width:32 color:ccGREEN textureFilename:@"streak.png"];
    streak_ = [CCMotionStreak streakWithFade:15 minSeg:3 width:3 color:ccWHITE textureFilename:@"dot.png"];
	[self addChild:streak_];
    
	// schedule an update on each frame so we can syncronize the streak with the target
	[self schedule:@selector(onUpdate:)];
    
	id a1 = [CCRotateBy actionWithDuration:2.7f angle:360];
	id action1 = [CCRepeatForever actionWithAction:a1];
    
	id a2 = [CCRotateBy actionWithDuration:7.4f angle:360];
	id action2 = [CCRepeatForever actionWithAction:a2];
    
	[centerNode runAction:[CCRepeatForever actionWithAction:action2]];
	[root runAction:[CCRepeatForever actionWithAction:action1]];
    
    
    CCActionInterval *colorAction = [CCRepeatForever actionWithAction:[CCSequence actions:
                                                                       [CCTintTo actionWithDuration:4.5f red:255 green:255 blue:255],
                                                                       [CCTintTo actionWithDuration:4.5f red:80 green:255 blue:204],
                                                                       [CCTintTo actionWithDuration:4.5f red:49 green:164 blue:131],
                                                                       [CCTintTo actionWithDuration:4.5f red:160 green:222 blue:222],
                                                                       [CCTintTo actionWithDuration:4.5f red:131 green:213 blue:213],
                                                                       [CCTintTo actionWithDuration:4.5f red:102 green:143 blue:204],
                                                                       [CCTintTo actionWithDuration:4.5f red:0 green:153 blue:124],nil
                                                                       ]
                                     ];
    [streak_ runAction:colorAction];
}

- (void)onUpdate:(ccTime)delta
{
	[streak_ setPosition:[target convertToWorldSpace:CGPointZero]];
}

// on "init" you need to initialize your instance
-(id) init
{
    self = [super initWithColor:ccc4(247, 249, 249, 255)];
//    self = [super init];
    center = CGPointMake(160, 210);
    
//    self.anim = [[Base8Animator alloc]initWithCenter:center];
//    dots = self.anim.drawPoints;
    counter = animIndex = 0;
    
    self.anim = [[Base8Animator alloc]initWithCenter:center andIndex:0 ofTotal:8];
    dots1 = self.anim.drawPoints;
    
    self.anim = [[Base8Animator alloc]initWithCenter:center andIndex:1 ofTotal:8];
    dots2 = self.anim.drawPoints;
    
    self.anim = [[Base8Animator alloc]initWithCenter:center andIndex:2 ofTotal:8];
    dots3 = self.anim.drawPoints;
    
    self.anim = [[Base8Animator alloc]initWithCenter:center andIndex:3 ofTotal:8];
    dots4 = self.anim.drawPoints;
    
    self.anim = [[Base8Animator alloc]initWithCenter:center andIndex:4 ofTotal:8];
    dots5 = self.anim.drawPoints;
    
    self.anim = [[Base8Animator alloc]initWithCenter:center andIndex:5 ofTotal:8];
    dots6 = self.anim.drawPoints;
    
    self.anim = [[Base8Animator alloc]initWithCenter:center andIndex:6 ofTotal:8];
    dots7 = self.anim.drawPoints;
    
    self.anim = [[Base8Animator alloc]initWithCenter:center andIndex:7 ofTotal:8];
    dots8 = self.anim.drawPoints;

	
	return self;
}

- (void)update: (ccTime) dt //add update method to your layer that will be called every simulation step
{
}

-(void) draw
{
    [super draw];
    
    /*
    
    ccGLEnableVertexAttribs( kCCVertexAttribFlag_Position );

    kmGLPushMatrix();
	ccDrawColor4B(0, 117, 153, 255);
    
    for (int i=0; i<8; i++) {
        
        self.anim = [[Base8Animator alloc]initWithCenter:center andIndex:i ofTotal:8];
        dots = self.anim.drawPoints;
        
        CGPoint origin = center;
        for (NSValue *value in dots) {
            
            CGPoint dest = [value CGPointValue];
            if (origin.x != center.x || origin.y != center.y) {
                ccDrawLine(origin, dest);// * CC_CONTENT_SCALE_FACTOR();
            }
            origin = dest;
        }
        
        ccDrawLine(origin, [[dots objectAtIndex:0] CGPointValue]);   
    }
	
    kmGLPopMatrix();
    counter++;
    
    NSLog(@"%d", counter);
    
    if (counter > 0) {
//        self.anim = [[Base8Animator alloc]initWithCenter:center];
//        dots = self.anim.drawPoints;
        counter = 0;
    }
    
    */
}
@end
