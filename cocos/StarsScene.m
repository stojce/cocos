//
//  StarsScene.m
//  cocos
//
//  Created by Stojce Slavkovski on 5/18/13.
//  Copyright (c) 2013 nextsense. All rights reserved.
//

#import "StarsScene.h"
#import "Base8Animator.h"

@interface StarsScene() {
    
    int counter;
    int animIndex;
    CGPoint center;
    
    NSArray *dots1;
    NSArray *dots2;
    NSArray *dots3;
    NSArray *dots4;
    NSArray *dots5;
    NSArray *dots6;
    NSArray *dots7;
    NSArray *dots8;
    
    CCSprite *target1;
    CCSprite *target2;
    CCSprite *target3;
    CCSprite *target4;
    CCSprite *target5;
    CCSprite *target6;
    CCSprite *target7;
    CCSprite *target8;
    
	CCMotionStreak *streak1_;
	CCMotionStreak *streak2_;
	CCMotionStreak *streak3_;
	CCMotionStreak *streak4_;
	CCMotionStreak *streak5_;
	CCMotionStreak *streak6_;
	CCMotionStreak *streak7_;
	CCMotionStreak *streak8_;
}

@property (strong, nonatomic)Base8Animator *anim;

@end

@implementation StarsScene

+(id) scene
{
    CCScene *scene = [CCScene node];
    StarsScene *layer = [StarsScene node];
    [scene addChild:layer];
    return scene;
}

// on "init" you need to initialize your instance
-(id) init
{
    self = [super initWithColor:ccc4(247, 249, 249, 255)];
    //    self = [super init];
    center = CGPointMake(160, 220);
    
    //    self.anim = [[Base8Animator alloc]initWithCenter:center];
    //    dots = self.anim.drawPoints;
    counter = animIndex = 0;
    
    self.anim = [[Base8Animator alloc]initWithCenter:center andIndex:0 ofTotal:8];
    dots1 = self.anim.drawPoints;
    
    self.anim = [[Base8Animator alloc]initWithCenter:center andIndex:1 ofTotal:8];
    dots2 = self.anim.drawPoints;
    
    self.anim = [[Base8Animator alloc]initWithCenter:center andIndex:2 ofTotal:8];
    dots3 = self.anim.drawPoints;
    
    self.anim = [[Base8Animator alloc]initWithCenter:center andIndex:3 ofTotal:8];
    dots4 = self.anim.drawPoints;
    
    self.anim = [[Base8Animator alloc]initWithCenter:center andIndex:4 ofTotal:8];
    dots5 = self.anim.drawPoints;
    
    self.anim = [[Base8Animator alloc]initWithCenter:center andIndex:5 ofTotal:8];
    dots6 = self.anim.drawPoints;
    
    self.anim = [[Base8Animator alloc]initWithCenter:center andIndex:6 ofTotal:8];
    dots7 = self.anim.drawPoints;
    
    self.anim = [[Base8Animator alloc]initWithCenter:center andIndex:7 ofTotal:8];
    dots8 = self.anim.drawPoints;
    
	return self;
}


- (void)onEnter
{
    [super onEnter];
	// schedule an update on each frame so we can syncronize the streak with the target
	[self schedule:@selector(onUpdate:)];
    ccTime duration = 7;
    
    streak1_ = [CCMotionStreak streakWithFade:duration minSeg:1 width:2 color:ccWHITE textureFilename:@"streak.png"];
    streak2_ = [CCMotionStreak streakWithFade:duration minSeg:1 width:2 color:ccWHITE textureFilename:@"streak.png"];
    streak3_ = [CCMotionStreak streakWithFade:duration minSeg:1 width:2 color:ccWHITE textureFilename:@"streak.png"];
    streak4_ = [CCMotionStreak streakWithFade:duration minSeg:1 width:2 color:ccWHITE textureFilename:@"streak.png"];
    streak5_ = [CCMotionStreak streakWithFade:duration minSeg:1 width:2 color:ccWHITE textureFilename:@"streak.png"];
    streak6_ = [CCMotionStreak streakWithFade:duration minSeg:1 width:2 color:ccWHITE textureFilename:@"streak.png"];
    streak7_ = [CCMotionStreak streakWithFade:duration minSeg:1 width:2 color:ccWHITE textureFilename:@"streak.png"];
    streak8_ = [CCMotionStreak streakWithFade:duration minSeg:1 width:2 color:ccWHITE textureFilename:@"streak.png"];

	[self addChild:streak1_];
    [self addChild:streak2_];
    [self addChild:streak3_];
    [self addChild:streak4_];
    [self addChild:streak5_];
    [self addChild:streak6_];
    [self addChild:streak7_];
    [self addChild:streak8_];
    
    
    /********  star 1 ********/
	CCPointArray *array1 = [CCPointArray arrayWithCapacity:30];
    for (NSValue *value in dots1) {
        [array1 addControlPoint:[value CGPointValue]];
    }
    [array1 addControlPoint:[[array1.controlPoints objectAtIndex:0] CGPointValue]];
	CCCatmullRomTo *action1 = [CCCardinalSplineTo actionWithDuration:duration points:array1 tension:1];
    
    /********  star 2 ********/
	CCPointArray *array2 = [CCPointArray arrayWithCapacity:dots2.count + 1];
    for (NSValue *value in dots2) {
        [array2 addControlPoint:[value CGPointValue]];
    }
    [array2 addControlPoint:[[array2.controlPoints objectAtIndex:0] CGPointValue]];
	CCCatmullRomTo *action2 = [CCCardinalSplineTo actionWithDuration:duration+0.5f points:array2 tension:1];
    
    /********  star 3 ********/
	CCPointArray *array3 = [CCPointArray arrayWithCapacity:dots3.count + 1];
    for (NSValue *value in dots3) {
        [array3 addControlPoint:[value CGPointValue]];
    }
	CCCatmullRomTo *action3 = [CCCardinalSplineTo actionWithDuration:duration+1 points:array3 tension:1];
    
    /********  star 4 ********/
	CCPointArray *array4 = [CCPointArray arrayWithCapacity:dots4.count + 1];
    for (NSValue *value in dots4) {
        [array4 addControlPoint:[value CGPointValue]];
    }
	CCCatmullRomTo *action4 = [CCCardinalSplineTo actionWithDuration:duration+1.5f points:array4 tension:1];
    
    /********  star 5 ********/
	CCPointArray *array5 = [CCPointArray arrayWithCapacity:dots5.count + 1];
    for (NSValue *value in dots5) {
        [array5 addControlPoint:[value CGPointValue]];
    }
	CCCatmullRomTo *action5 = [CCCardinalSplineTo actionWithDuration:duration points:array5 tension:1];
    
    /********  star 6 ********/
	CCPointArray *array6 = [CCPointArray arrayWithCapacity:dots6.count + 1];
    for (NSValue *value in dots6) {
        [array6 addControlPoint:[value CGPointValue]];
    }
	CCCatmullRomTo *action6 = [CCCardinalSplineTo actionWithDuration:duration+0.5f points:array6 tension:1];
    
    /********  star 7 ********/
	CCPointArray *array7 = [CCPointArray arrayWithCapacity:dots7.count + 1];
    for (NSValue *value in dots7) {
        [array7 addControlPoint:[value CGPointValue]];
    }
	CCCatmullRomTo *action7 = [CCCardinalSplineTo actionWithDuration:duration+1 points:array7 tension:1];
    
    /********  star 8 ********/
	CCPointArray *array8 = [CCPointArray arrayWithCapacity:dots8.count + 1];
    for (NSValue *value in dots8) {
        [array8 addControlPoint:[value CGPointValue]];
    }
	CCCatmullRomTo *action8 = [CCCardinalSplineTo actionWithDuration:duration+1.5f points:array8 tension:1];
    
    /********  color action ********/
    CCActionInterval *colorAction1 = [CCRepeatForever actionWithAction:[CCSequence actions:
                                                                       [CCTintTo actionWithDuration:1.5f red:0 green:255 blue:0],
                                                                       [CCTintTo actionWithDuration:1.5f red:49 green:164 blue:131],
                                                                       [CCTintTo actionWithDuration:1.5f red:160 green:222 blue:222],
                                                                       [CCTintTo actionWithDuration:1.5f red:0 green:0 blue:255],
                                                                       [CCTintTo actionWithDuration:1.5f red:131 green:213 blue:213],
                                                                       [CCTintTo actionWithDuration:1.5f red:102 green:143 blue:204],
                                                                       [CCTintTo actionWithDuration:1.5f red:255 green:0 blue:0],
                                                                       [CCTintTo actionWithDuration:1.5f red:26 green:255 blue:203],
                                                                       [CCTintTo actionWithDuration:1.5f red:102 green:204 blue:21],
                                                                       nil]
                                     ];
    
    
    /********  color action2 ********/
    CCActionInterval *colorAction2 = [CCRepeatForever actionWithAction:[CCSequence actions:
                                                                        [CCTintTo actionWithDuration:1.5f red:0 green:255 blue:0],
                                                                        [CCTintTo actionWithDuration:1.5f red:49 green:164 blue:131],
                                                                        [CCTintTo actionWithDuration:1.5f red:160 green:222 blue:222],
                                                                        [CCTintTo actionWithDuration:1.5f red:0 green:0 blue:255],
                                                                        [CCTintTo actionWithDuration:1.5f red:131 green:213 blue:213],
                                                                        [CCTintTo actionWithDuration:1.5f red:102 green:143 blue:204],
                                                                        [CCTintTo actionWithDuration:1.5f red:255 green:0 blue:0],
                                                                        [CCTintTo actionWithDuration:1.5f red:26 green:255 blue:203],
                                                                        [CCTintTo actionWithDuration:1.5f red:102 green:204 blue:21],
                                                                        nil]
                                      ];
    /********  color action3 ********/
    CCActionInterval *colorAction3 = [CCRepeatForever actionWithAction:[CCSequence actions:
                                                                        [CCTintTo actionWithDuration:0.5f red:0 green:255 blue:0],
                                                                        [CCTintTo actionWithDuration:1.5f red:49 green:164 blue:131],
                                                                        [CCTintTo actionWithDuration:0.5f red:160 green:222 blue:222],
                                                                        [CCTintTo actionWithDuration:0.5f red:0 green:0 blue:255],
                                                                        [CCTintTo actionWithDuration:1.5f red:131 green:213 blue:213],
                                                                        [CCTintTo actionWithDuration:1.5f red:102 green:143 blue:204],
                                                                        [CCTintTo actionWithDuration:0.5f red:255 green:0 blue:0],
                                                                        [CCTintTo actionWithDuration:1.5f red:26 green:255 blue:203],
                                                                        [CCTintTo actionWithDuration:1.5f red:102 green:204 blue:21],
                                                                        nil]
                                      ];
    /********  color action4 ********/
    CCActionInterval *colorAction4 = [CCRepeatForever actionWithAction:[CCSequence actions:
                                                                        [CCTintTo actionWithDuration:1.5f red:0 green:255 blue:0],
                                                                        [CCTintTo actionWithDuration:4.5f red:49 green:164 blue:131],
                                                                        [CCTintTo actionWithDuration:1.5f red:160 green:222 blue:222],
                                                                        [CCTintTo actionWithDuration:1.5f red:0 green:0 blue:255],
                                                                        [CCTintTo actionWithDuration:1.5f red:131 green:213 blue:213],
                                                                        [CCTintTo actionWithDuration:1.5f red:102 green:143 blue:204],
                                                                        [CCTintTo actionWithDuration:4.5f red:255 green:0 blue:0],
                                                                        [CCTintTo actionWithDuration:1.5f red:26 green:255 blue:203],
                                                                        [CCTintTo actionWithDuration:1.5f red:102 green:204 blue:21],
                                                                        nil]
                                      ];
    /********  color action5 ********/
    CCActionInterval *colorAction5 = [CCRepeatForever actionWithAction:[CCSequence actions:
                                                                        [CCTintTo actionWithDuration:1.5f red:0 green:255 blue:0],
                                                                        [CCTintTo actionWithDuration:2.5f red:49 green:164 blue:131],
                                                                        [CCTintTo actionWithDuration:1.5f red:160 green:222 blue:222],
                                                                        [CCTintTo actionWithDuration:1.5f red:0 green:0 blue:255],
                                                                        [CCTintTo actionWithDuration:1.5f red:131 green:213 blue:213],
                                                                        [CCTintTo actionWithDuration:0.5f red:102 green:143 blue:204],
                                                                        [CCTintTo actionWithDuration:1.5f red:255 green:0 blue:0],
                                                                        [CCTintTo actionWithDuration:1.5f red:26 green:255 blue:203],
                                                                        [CCTintTo actionWithDuration:1.5f red:102 green:204 blue:21],
                                                                        nil]
                                      ];
    /********  color action6 ********/
    CCActionInterval *colorAction6 = [CCRepeatForever actionWithAction:[CCSequence actions:
                                                                        [CCTintTo actionWithDuration:1.5f red:0 green:255 blue:0],
                                                                        [CCTintTo actionWithDuration:1.5f red:49 green:164 blue:131],
                                                                        [CCTintTo actionWithDuration:1.5f red:160 green:222 blue:222],
                                                                        [CCTintTo actionWithDuration:1.5f red:0 green:0 blue:255],
                                                                        [CCTintTo actionWithDuration:1.5f red:131 green:213 blue:213],
                                                                        [CCTintTo actionWithDuration:1.5f red:102 green:143 blue:204],
                                                                        [CCTintTo actionWithDuration:1.5f red:255 green:0 blue:0],
                                                                        [CCTintTo actionWithDuration:1.5f red:26 green:255 blue:203],
                                                                        [CCTintTo actionWithDuration:1.5f red:102 green:204 blue:21],
                                                                        nil]
                                      ];
    /********  color action7 ********/
    CCActionInterval *colorAction7 = [CCRepeatForever actionWithAction:[CCSequence actions:
                                                                        [CCTintTo actionWithDuration:1.5f red:0 green:255 blue:0],
                                                                        [CCTintTo actionWithDuration:1.5f red:49 green:164 blue:131],
                                                                        [CCTintTo actionWithDuration:1.5f red:160 green:222 blue:222],
                                                                        [CCTintTo actionWithDuration:1.5f red:0 green:0 blue:255],
                                                                        [CCTintTo actionWithDuration:1.5f red:131 green:213 blue:213],
                                                                        [CCTintTo actionWithDuration:1.5f red:102 green:143 blue:204],
                                                                        [CCTintTo actionWithDuration:1.5f red:255 green:0 blue:0],
                                                                        [CCTintTo actionWithDuration:1.5f red:26 green:255 blue:203],
                                                                        [CCTintTo actionWithDuration:1.5f red:102 green:204 blue:21],
                                                                        nil]
                                      ];
    /********  color action8 ********/
    CCActionInterval *colorAction8 = [CCRepeatForever actionWithAction:[CCSequence actions:
                                                                        [CCTintTo actionWithDuration:1.5f red:0 green:255 blue:0],
                                                                        [CCTintTo actionWithDuration:1.5f red:49 green:164 blue:131],
                                                                        [CCTintTo actionWithDuration:1.5f red:160 green:222 blue:222],
                                                                        [CCTintTo actionWithDuration:1.5f red:0 green:0 blue:255],
                                                                        [CCTintTo actionWithDuration:1.5f red:131 green:213 blue:213],
                                                                        [CCTintTo actionWithDuration:1.5f red:102 green:143 blue:204],
                                                                        [CCTintTo actionWithDuration:1.5f red:255 green:0 blue:0],
                                                                        [CCTintTo actionWithDuration:1.5f red:26 green:255 blue:203],
                                                                        [CCTintTo actionWithDuration:1.5f red:102 green:204 blue:21],
                                                                        nil]
                                      ];
    
    target1 = [[CCSprite alloc] init];
    [self addChild:target1 z:1];
    action1.tag = 1001;
    [target1 runAction:[CCRepeatForever actionWithAction:action1]];
    
    target2 = [[CCSprite alloc] init];
    [self addChild:target2 z:1];
    action2.tag = 1002;
    [target2 runAction:[CCRepeatForever actionWithAction:action2]];
    
    target3 = [[CCSprite alloc] init];
    [self addChild:target3 z:1];
    action3.tag = 1003;
    [target3 runAction:[CCRepeatForever actionWithAction:action3]];
    
    target4 = [[CCSprite alloc] init];
    [self addChild:target4 z:1];
    action4.tag = 1004;
    [target4 runAction:[CCRepeatForever actionWithAction:action4]];
    
    target5 = [[CCSprite alloc] init];
    [self addChild:target5 z:1];
    action5.tag = 1005;
    [target5 runAction:[CCRepeatForever actionWithAction:action5]];
    
    target6 = [[CCSprite alloc] init];
    [self addChild:target6 z:1];
    action6.tag = 1006;
    [target6 runAction:[CCRepeatForever actionWithAction:action6]];
    
    target7 = [[CCSprite alloc] init];
    [self addChild:target7 z:1];
    action7.tag = 1007;
    [target7 runAction:[CCRepeatForever actionWithAction:action7]];
    
    target8 = [[CCSprite alloc] init];
    [self addChild:target8 z:1];
    action8.tag = 1008;
    [target8 runAction:[CCRepeatForever actionWithAction:action8]];
    
    
    [streak1_ runAction:colorAction1];
    [streak2_ runAction:colorAction2];
    [streak3_ runAction:colorAction3];
    [streak4_ runAction:colorAction4];
    [streak5_ runAction:colorAction5];
    [streak6_ runAction:colorAction6];
    [streak7_ runAction:colorAction7];
    [streak8_ runAction:colorAction8];
}

- (void)onUpdate:(ccTime)delta
{
	[streak1_ setPosition:[target1 convertToWorldSpace:CGPointZero]];
	[streak2_ setPosition:[target2 convertToWorldSpace:CGPointZero]];
	[streak3_ setPosition:[target3 convertToWorldSpace:CGPointZero]];
	[streak4_ setPosition:[target4 convertToWorldSpace:CGPointZero]];
	[streak5_ setPosition:[target5 convertToWorldSpace:CGPointZero]];
	[streak6_ setPosition:[target6 convertToWorldSpace:CGPointZero]];
	[streak7_ setPosition:[target7 convertToWorldSpace:CGPointZero]];
	[streak8_ setPosition:[target8 convertToWorldSpace:CGPointZero]];
}

@end
