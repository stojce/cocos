//
//  AppDelegate.h
//  cocos
//
//  Created by Stojce Slavkovski on 5/14/13.
//  Copyright (c) 2013 nextsense. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
