//
//  MyScene.h
//  cocos
//
//  Created by Stojce Slavkovski on 5/14/13.
//  Copyright (c) 2013 nextsense. All rights reserved.
//

#import "cocos2d.h"

@interface MyScene : CCLayerColor

+(id) scene;

@end
