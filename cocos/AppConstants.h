//
//  AppConstants.h
//  cocos
//
//  Created by Stojce Slavkovski on 5/18/13.
//  Copyright (c) 2013 nextsense. All rights reserved.
//

#define kStarPoints 5
#define kStars 8

#define kLowMinRadius 30
#define kLowMaxRadius 60
#define kHighMinRadius 80
#define kHighMaxRadius 100
