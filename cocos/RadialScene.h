//
//  RadialScene.h
//  cocos
//
//  Created by Stojce Slavkovski on 5/18/13.
//  Copyright (c) 2013 nextsense. All rights reserved.
//

#import "cocos2d.h"

@interface RadialScene : CCLayerColor

+(id)scene;

@end
