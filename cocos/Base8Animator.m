//
//  Base8Animator.m
//  cocos
//
//  Created by Stojce Slavkovski on 5/18/13.
//  Copyright (c) 2013 nextsense. All rights reserved.
//

#import "Base8Animator.h"

@interface Base8Animator()

@property (nonatomic)int numberOfDots;
@property (nonatomic)int lowRadius;
@property (nonatomic)int highRadius;
@property (nonatomic)color4F lineColor;

@end

@implementation Base8Animator

- (id)init
{
    self = [super init];
    if (self != nil) {
    }
    return self;
}

- (id)initWithCenter:(CGPoint)center andIndex:(int)index ofTotal:(int)total
{
    if (total <= index) {
        [NSException raise:@"Invalid index value" format:@"index [%d] is greater or equal to %d", index, total];
        return nil;
    }
    
    self = [super init];
    if (self != nil) {
        
        self.numberOfDots = kStarPoints * 2;
        int rand1 = abs(arc4random());
        self.lowRadius = (rand1 % (kLowMaxRadius - kLowMinRadius)) + kLowMinRadius;
        self.highRadius = (rand1 % (kHighMaxRadius - kHighMinRadius)) + kHighMinRadius;
        
        self.lineColor = (color4F){0, 117, 153, 255};
        
        // angle of progression
        const float coef = 2.0f * (float)M_PI / self.numberOfDots;
    
        NSMutableArray *tmpArray = [[NSMutableArray alloc] init];

        
        // pitch
        float startcoef = index * 2.0f * (float)M_PI / total;
        NSLog(@"rand1: %d", rand1);
        NSLog(@"low: %d", self.lowRadius);
        NSLog(@"high: %d", self.highRadius);
        NSLog(@"startcoef: %f", startcoef);
        
        if (self.lowRadius < kLowMinRadius || self.lowRadius > kLowMaxRadius) {
            NSLog(@"low: %d", self.lowRadius);
        }
        
        if (self.highRadius < kHighMinRadius || self.highRadius > kHighMaxRadius) {
            NSLog(@"high: %d", self.highRadius);
        }
        
        for (NSUInteger i = 0; i <= self.numberOfDots - 1; i++) {
            float rads = i * coef + startcoef;
            
            int radius = i % 2 == 0 ? self.lowRadius : self.highRadius;
            
            GLfloat j = radius * cosf(rads + 0) + center.x;
            GLfloat k = radius * sinf(rads + 0) + center.y;
            
            NSValue *val = [NSValue valueWithCGPoint:CGPointMake(j, k)];
            [tmpArray addObject:val];
        }
        
        _drawPoints = [NSArray arrayWithArray:tmpArray];
    }
    
    NSLog(@"%@", self.drawPoints);
    return self;
}

@end
