//
//  ViewController.h
//  cocos
//
//  Created by Stojce Slavkovski on 5/14/13.
//  Copyright (c) 2013 nextsense. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "cocos2d.h"

typedef enum {
    AnimTypeStars,
    AnimTypeCircles
}
AnimType;

@interface ViewController : UIViewController <CCDirectorDelegate>

@property (nonatomic)AnimType animType;

@end
